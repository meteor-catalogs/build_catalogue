# Constructeur automatisé de catalogue pour Meteor

## Introduction
Le dépot d'un catalogue pour meteor doit respecter quelques mesures:
1. Le catalogue devra présenter une fiche descriptive au format json (nommé description.json) respectant le tag indiqués par [zenodo](https://developers.zenodo.org/#quickstart-upload). Valider le json avec: https://jsonlint.com/
2. Les fichiers index bowtie2 ne doivent pas être publiés. Ajouter en conséquence créer un fichier .gitignore: 
```
*.bt2
*.bt2l
```
3. Les fichiers fasta doivent être suivi via git-lfs
4. Le dépôt doit contenir un fichier .gitlab-ci.yml qui permet de déclencher la CI:
```
trigger_build:
  stage: build
  variables:
    UPSTREAM_REPOSITORY_URL: $CI_PROJECT_URL
    UPSTREAM_DESCRIPTION_ID: $DESCRIPTION_ID
    UPSTREAM_CI_PROJECT_NAME: $CI_PROJECT_NAME
  trigger:
    project: meteor-catalogs/build_catalogue
    strategy: depend
```
5. A la première soumission, l'integration continue va créer la fiche sur git, tel que: https://sandbox.zenodo.org/uploads/23305
6. Afin de mettre à jour ce projet, le dépôt du catalogue devra référencer l'ID du projet:  23305. Pour cela, créer une variable DESCRIPTION_ID dans settings -> CI/CD -> variables avec la valeur 23305.

## Utilisation

Dans le dossier du catalogue, réaliser les opérations suivantes:
```
# Initialisation de git
git init
# Creation du git attributes
git lfs track "*.fasta"
# Ajouter le lien vers le dépot forgemia
git remote add origin ...
# Ajoute les fichers du CI
git add .git*
# Ajouter les fichiers et Versionner
git commit -am "Initial commit"
# Push
git lfs push origin main
git push --set-upstream origin main
```

## Contact

En cas de questions, vous pouvez me contacter par email: amine.ghozlane[at]pasteur.fr
