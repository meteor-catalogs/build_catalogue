#!/bin/env python3
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    A copy of the GNU General Public License is available at
#    http://www.gnu.org/licenses/gpl-3.0.html

import requests
import json
import argparse
from pathlib import Path
import time

__author__ = "Amine Ghozlane"
__copyright__ = "Copyright 2024, Institut Pasteur"
__credits__ = ["Amine Ghozlane"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Amine Ghozlane"
__email__ = "amine.ghozlane@pasteur.fr"
__status__ = "Developpement"


def get_arguments():  # pragma: no cover
    """
    Meteor help and arguments

    No arguments

    Return : parser
    """
    # Create an ArgumentParser object
    parser = argparse.ArgumentParser(description="Post a catalogue to zenodo.")

    # Add a positional argument for the string parameter
    parser.add_argument(
        "-i",
        dest="catalogue_list",
        nargs="+",
        type=Path,
        required=True,
        help="Catalogue file",
    )
    parser.add_argument(
        "-t", dest="access_token", type=str, required=True, help="Access token"
    )
    parser.add_argument(
        "-j", dest="json_file", type=Path, help="Configuration of the catalogue"
    )
    parser.add_argument(
        "-s",
        dest="sandbox",
        action="store_true",
        help="Send data to zenodo sandbox",
    )
    parser.add_argument(
        "description_id", nargs="?", type=int, help="Description id of the deposition"
    )
    return parser.parse_args()


def send_post_with_retry(
    deposition, access_token, headers, max_retries=10, retry_delay=5
):
    for attempt in range(1, max_retries + 1):
        try:
            response = requests.post(
                deposition,
                params={"access_token": access_token},
                json={},
                headers=headers,
            )
            # Check if the request was successful (you can customize this based on your needs)
            if response.status_code in [200, 201, 202, 204]:
                print("Post sent successfully!")
                break  # Exit the loop if successful
            else:
                print(f"Attempt {attempt} failed. Retrying in {retry_delay} seconds.")
        except Exception as e:
            print(r.status_code)
            print(r.text)
            print(r)
            print(f"Error: {e}. Retrying in {retry_delay} seconds.")
        time.sleep(retry_delay)  # Wait for the specified delay before the next attempt

    else:
        print(f"Failed after {max_retries} attempts. Data not sent.")
    return response


def main():
    sandbox = ""
    # Parse the command-line arguments
    args = get_arguments()
    headers = {"Content-Type": "application/json"}
    if args.json_file:
        with args.json_file.open("r") as json_data:
            data = json.load(json_data)
    else:
        data = {}
    if args.sandbox:
        sandbox = "sandbox."
    if args.description_id:
        deposition = (
            f"https://{sandbox}zenodo.org/api/deposit/depositions/{args.description_id}"
        )
    else:
        deposition = f"https://{sandbox}zenodo.org/api/deposit/depositions"
        r = send_post_with_retry(deposition, args.access_token, headers)
        print(f"Deposition id {r.json()['id']}")
        deposition = (
            f"https://{sandbox}zenodo.org/api/deposit/depositions/{r.json()['id']}"
        )
    # Post description
    r = requests.put(
        deposition,
        params={"access_token": args.access_token},
        data=json.dumps(data),
        headers=headers,
    )
    # Post data
    if r.status_code in [200, 201, 202, 204]:
        # deposition_id = r.json()["id"]
        bucket_url = r.json()["links"]["bucket"]
        for catalogue in args.catalogue_list:
            with catalogue.open("rb") as fp:
                r = requests.put(
                    f"{bucket_url}/{catalogue.name}",
                    data=fp,
                    params={"access_token": args.access_token},
                )
    else:
        print("Submission failed")
        print(r.status_code)
        print(r.text)


if __name__ == "__main__":
    main()
