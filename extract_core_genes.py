#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    A copy of the GNU General Public License is available at
#    http://www.gnu.org/licenses/gpl-3.0.html

import sys
import argparse
import pandas as pd
from pathlib import Path

__author__ = "Amine Ghozlane"
__copyright__ = "Copyright 2024, Institut Pasteur"
__credits__ = ["Amine Ghozlane"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Amine Ghozlane"
__email__ = "amine.ghozlane@pasteur.fr"
__status__ = "Developpement"


def get_arguments():
    """Retrieves the arguments of the program.
    Returns: An object that contains the arguments
    """
    # Parsing arguments
    parser = argparse.ArgumentParser(
        description=__doc__, usage="{0} -h".format(sys.argv[0])
    )
    parser.add_argument(
        "-i",
        dest="core_gene_file",
        type=Path,
        required=True,
        default="",
        help="Core gene annotation.",
    )
    parser.add_argument(
        "-n",
        dest="core_gene_num",
        type=int,
        default=100,
        help="Number of core gene taken in account.",
    )
    parser.add_argument(
        "-o",
        dest="output_file",
        type=Path,
        default=Path("output_file.tsv"),
        help="Output file.",
    )
    args = parser.parse_args()
    return args


def main():
    """Main program"""
    args = get_arguments()
    if not args.core_gene_file.exists():
        sys.exit(f"File {args.core_gene_file} is missing")
    msp_content = pd.read_csv(args.core_gene_file, delimiter="\t")
    msp_content = msp_content.loc[msp_content["gene_category"] == "core"]
    msp_content = msp_content.groupby("msp_name").head(args.core_gene_num).reset_index()
    msp_content.to_csv(
        args.output_file, columns=["gene_id"], sep="\t", header=False, index=False
    )


if __name__ == "__main__":
    main()
