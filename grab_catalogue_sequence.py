#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    A copy of the GNU General Public License is available at
#    http://www.gnu.org/licenses/gpl-3.0.html

"""Extract a list of sequence from the catalogue."""


import argparse
import sys
import os
import csv
from pathlib import Path
import bisect
import textwrap
import gzip
from typing import List, Dict

__author__ = "Amine Ghozlane"
__copyright__ = "Copyright 2024, Institut Pasteur"
__credits__ = ["Amine Ghozlane"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Amine Ghozlane"
__email__ = "amine.ghozlane@pasteur.fr"
__status__ = "Developpement"


def isfile(path: str) -> Path:  # pragma: no cover
    """Check if path is an existing file.

    :param path: (str) Path to the file

    :raises ArgumentTypeError: If file does not exist

    :return: (Path) Path object of the input file
    """
    myfile = Path(path)
    if not myfile.is_file():
        if myfile.is_dir():
            msg = f"{myfile.name} is a directory."
        else:
            msg = f"{myfile.name} does not exist."
        raise argparse.ArgumentTypeError(msg)
    return myfile


def isdir(path: str) -> Path:  # pragma: no cover
    """Check if path can be valid directory.

    :param path: Path to the directory

    :raises ArgumentTypeError: If directory does not exist

    :return: (str) Path object of the directory
    """
    mydir = Path(path)
    # if not mydir.is_dir():
    if mydir.is_file():
        msg = f"{mydir.name} is a file."
        raise argparse.ArgumentTypeError(msg)
        # else:
        #     msg = f"{mydir.name} does not exist."
    return mydir


def getArguments():
    """Retrieves the arguments of the program.
    Returns: An object that contains the arguments
    """
    # Parsing arguments
    parser = argparse.ArgumentParser(
        description=__doc__, usage="{0} -h".format(sys.argv[0])
    )
    parser.set_defaults(results=".{0}".format(os.sep))
    parser.add_argument(
        "-i",
        dest="list_sequences_file",
        type=isfile,
        required=True,
        help="List of sequence to extract.",
    )
    parser.add_argument(
        "-d", dest="catalogue_file", type=isfile, required=True, help="Database query."
    )
    parser.add_argument(
        "-n",
        dest="not_in_database",
        action="store_true",
        help="Select instead elements which are not in the" " list.",
    )
    parser.add_argument("-o", dest="output_file", type=Path, help="Output file.")
    parser.add_argument(
        "-r", dest="results", type=isdir, help="Path to result directory."
    )
    return parser.parse_args()


def extract_interest_elements(list_sequences_file: Path) -> List[str]:
    """Get a list of the element of interest"""
    list_sequences = []
    try:
        with list_sequences_file.open("rt") as list_seq:
            list_sequences_reader = csv.reader(list_seq)
            for line in list_sequences_reader:
                list_sequences.append(line[0].split(" ")[0].split("\t")[0])
            # print(list_sequences)
            assert len(list_sequences) > 0
            list_sequences.sort()
    except IOError:
        sys.exit("Error cannot the file : {0}".format(list_sequences_file))
    except AssertionError:
        sys.exit(
            "Error no element detected in the file : {0}".format(list_sequences_file)
        )
    return list_sequences


# def is_selected(header, list_sequences):
#     """
#     """
#     for element in list_sequences:
#         if element in header:
#             return element
#     return None


def get_element(name: str, input_list: List[str]) -> bool:
    """Search name in input list
    Arguments:
      input_list: List
      name: Search criteria
    """
    # Searching the node with its name
    i = bisect.bisect_left(input_list, name)
    # Object has been found
    if i != len(input_list) and input_list[i] == name:
        return True  # input_list[i]
    return False  # None


def extract_catalogue_sequence(
    list_sequences: List[str], catalogue_file: Path, not_in_database: bool
) -> Dict[str, str]:
    """ """
    grab_sequence = False
    interest_sequence = {}
    title = ""
    if catalogue_file.suffix == ".gz":
        catalogue = gzip.open(catalogue_file, "rt")
    else:
        catalogue = catalogue_file.open("rt")
    try:
        with catalogue:
            for line in catalogue:
                if line[0] == ">":
                    grab_sequence = False
                    title = line[1:].replace("\n", "").replace("\r", "")
                    if " " in title:
                        title = title.split(" ")[0]
                    selection = get_element(title, list_sequences)
                    if selection and not not_in_database:
                        interest_sequence[title] = ""
                        grab_sequence = True
                    elif not selection and not_in_database:
                        interest_sequence[title] = ""
                        grab_sequence = True
                elif grab_sequence and len(line) > 0:
                    interest_sequence[title] += line.replace("\n", "").replace("\r", "")
            assert len(interest_sequence) > 0
    except IOError:
        sys.exit("Error cannot open the file: {0}".format(catalogue_file))
    except AssertionError:
        sys.exit("Error no element detected in the file: {0}".format(catalogue_file))
    return interest_sequence


def write_interest_sequence(interest_sequence: Dict, output_file: Path):
    """ """
    if output_file.suffix == ".gz":
        output = gzip.open(output_file, "wt")
    else:
        output = output_file.open("wt")
    try:
        with output:
            for key in interest_sequence:
                output.write(
                    ">{1}{0}{2}{0}".format(
                        os.linesep, key, textwrap.fill(interest_sequence[key], width=80)
                    )
                )
    except IOError:
        sys.exit("Error cannot open {0}".format(output_file))


# ==============================================================
# Main program
# ==============================================================
def main():
    """
    Main program function
    """
    # Get the arguments
    args = getArguments()
    # Get List of sequence of interest
    print("Load the list of sequence of interest ...")
    list_sequences = extract_interest_elements(args.list_sequences_file)
    print("{0} sequences to search".format(len(list_sequences)))
    # print(list_sequences)
    # Extract catalogue sequence
    print("Extract sequences from the catalogue...")
    interest_sequence = extract_catalogue_sequence(
        list_sequences, args.catalogue_file, args.not_in_database
    )
    print("{0} extracted sequences".format(len(interest_sequence)))
    # Write sequences
    if not args.output_file:
        args.output_file = "extracted_sequence.fasta"
    print("Write sequences to {0}".format(args.output_file))
    write_interest_sequence(interest_sequence, args.output_file)
    print("Done.")


if __name__ == "__main__":
    main()
